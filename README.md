# Omnium

Copyright (c) 2021-present

Portions of this software are licensed as follows:

* All content residing under the "master/" directory of this repository is licensed under "MIT Expat".
* All third party components incorporated into the GitLab Software are licensed under the original license provided by the owner of the applicable component.
* Content outside of the above mentioned directories or restrictions above is available under the "MIT Expat" license as defined below.


