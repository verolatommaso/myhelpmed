import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { View, Image } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import SplashScreen from "react-native-splash-screen";
import AsyncStorage from "@react-native-community/async-storage";
// screens
import { OnBoarding } from "./app/screens/";
import HomeScreen from "@app/screens/HomeScreen";
import SecondScreen from "@app/screens/SecondScreen";
import ThirdScreen from "@app/screens/ThirdScreen";
import TerapiaIcon from "./app/assets/icons/TerapiaIcon";
import MeetingIcon from "./app/assets/icons/MeetingIcon";
import CuriositaIcon from "./app/assets/icons/CuriositaIcon";
import BellIcon from "./app/assets/icons/BellIcon";

import ConditionScreen from "@app/screens/ConditionScreen";
import { images } from "./app/constants";

//main stack navigator container to host two different types of navigator
const RootStack = createStackNavigator();

// screen for stack & tabs
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const { avatar } = images;

const getLogin = async () => {
  try {
    const value = await AsyncStorage.getItem("@authentication_data");
    if (value) {
      return await JSON.parse(value).login;
    } else {
      return null;
    }
  } catch (e) {
    return e;
  }
};

const App = () => {
  const [login, setLogin] = useState(null);

  useEffect(() => {
    SplashScreen.hide();
    const getCurrentCredentials = async () => {
      const currLogin = await getLogin();
      setLogin(currLogin);
    };
    getCurrentCredentials();
  }, [login]);

  const AppScreens = () => (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          switch (route.name) {
            case "Terapia": {
              return <TerapiaIcon fill={focused ? "#6A7AFF" : "#CBCBCB"} />;
            }
            case "Meeting": {
              return <MeetingIcon fill={focused ? "#6A7AFF" : "#CBCBCB"} />;
            }
            case "Curiosità": {
              return <CuriositaIcon fill={focused ? "#6A7AFF" : "#CBCBCB"} />;
            }
            default: {
              return <TerapiaIcon fill={focused ? "#6A7AFF" : "#CBCBCB"} />;
            }
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: "#6A7AFF",
        inactiveTintColor: "#CBCBCB",
        labelStyle: {
          fontFamily: "NunitoSans-Regular",
          fontSize: 13,
          position: "absolute",
          bottom: -13,
        },
        iconStyle: {
          marginTop: 9,
        },
      }}
    >
      <Tab.Screen name="Terapia" component={HomeScreen} />
      <Tab.Screen name="Meeting" component={SecondScreen} />
      <Tab.Screen name="Curiosità" component={ThirdScreen} />
    </Tab.Navigator>
  );

  const OnBoardingScreen = () => (
    <Stack.Navigator>
      {/* Onboarding screen */}
      <Stack.Screen
        name="ConditionScreen"
        component={ConditionScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OnBoarding"
        component={OnBoarding}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );

  const RootStackScreen = ({ login }) => {
    return login ? (
      <RootStack.Navigator>
        <RootStack.Screen
          options={{
            title: "",
            headerLeft: () => (
              <View style={{ marginLeft: 22 }}>
                <BellIcon />
              </View>
            ),
            headerRight: () => (
              <View style={{ marginRight: 32 }}>
                <Image source={avatar} style={{ width: 28, height: 28 }} />
              </View>
            ),
          }}
          name="AppScreens"
          component={AppScreens}
        />
      </RootStack.Navigator>
    ) : (
      <RootStack.Navigator>
        <RootStack.Screen
          name="OnBoardingScreen"
          component={OnBoardingScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name="AppScreens"
          component={AppScreens}
          options={{
            title: "",
            headerLeft: () => (
              <View style={{ marginLeft: 22 }}>
                <BellIcon />
              </View>
            ),
            headerRight: () => (
              <View style={{ marginRight: 32 }}>
                <Image source={avatar} style={{ width: 28, height: 28 }} />
              </View>
            ),
          }}
        />
      </RootStack.Navigator>
    );
  };

  return (
    <NavigationContainer>
      <RootStackScreen login={login} />
    </NavigationContainer>
  );
};

export default () => {
  return <App />;
};
