import React from "react";

import { SvgCss } from "react-native-svg";

const xml = `

<svg id="arrow_back_ios_black_24dp" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
  <path id="Tracciato_590" data-name="Tracciato 590" d="M0,0H24V24H0Z" fill="none" opacity="0.87"/>
  <path id="Tracciato_591" data-name="Tracciato 591" d="M17.51,3.87,15.73,2.1,5.84,12l9.9,9.9,1.77-1.77L9.38,12Z" fill="#15213d"/>
</svg>

`;

const ChevronIcon = ({ width, height }) => (
  <SvgCss xml={xml} width={width} height={height} />
);

export default ChevronIcon;
