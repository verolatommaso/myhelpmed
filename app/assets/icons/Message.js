
  
  import React from "react";

  import { SvgCss } from "react-native-svg";
  
  const xml = `
  <svg xmlns="http://www.w3.org/2000/svg" width="40.548" height="31.439" viewBox="0 0 40.548 31.439">
    <g id="COMMUNICATION" transform="translate(0 -14.378)">
      <path id="Tracciato_790" data-name="Tracciato 790" d="M32.907,57.661h-1.98v-1.98a.554.554,0,0,0-.554-.554H27.839a.554.554,0,0,0-.554.554v1.98H25.3a.554.554,0,0,0-.554.554V60.75a.554.554,0,0,0,.554.554h1.98v1.98a.554.554,0,0,0,.554.554h2.534a.554.554,0,0,0,.554-.554V61.3h1.98a.554.554,0,0,0,.554-.554V58.216A.554.554,0,0,0,32.907,57.661ZM32.353,60.2h-1.98a.554.554,0,0,0-.554.554v1.98H28.393V60.75a.554.554,0,0,0-.554-.554h-1.98V58.77h1.98a.554.554,0,0,0,.554-.554v-1.98h1.426v1.98a.554.554,0,0,0,.554.554h1.98Z" transform="translate(-16.91 -27.84)" fill="#fd9895"/>
      <path id="Tracciato_791" data-name="Tracciato 791" d="M78.5,24.378H61.554a.554.554,0,1,0,0,1.109H78.5a.554.554,0,1,0,0-1.109Z" transform="translate(-41.676 -6.832)" fill="#fd9895"/>
      <path id="Tracciato_792" data-name="Tracciato 792" d="M78.5,33.71H61.554a.554.554,0,1,0,0,1.109H78.5a.554.554,0,0,0,0-1.109Z" transform="translate(-41.676 -13.208)" fill="#fd9895"/>
      <path id="Tracciato_793" data-name="Tracciato 793" d="M93.88,43.042H84.062a.554.554,0,0,0,0,1.109H93.88a.554.554,0,1,0,0-1.109Z" transform="translate(-57.054 -19.584)" fill="#fd9895"/>
      <path id="Tracciato_794" data-name="Tracciato 794" d="M93.88,52.373H84.062a.554.554,0,0,0,0,1.109H93.88a.554.554,0,0,0,0-1.109Z" transform="translate(-57.054 -25.959)" fill="#fd9895"/>
      <path id="Tracciato_795" data-name="Tracciato 795" d="M38.41,14.378H18.294a2.141,2.141,0,0,0-2.138,2.138v6.969H2.138A2.141,2.141,0,0,0,0,25.624V37.66A2.14,2.14,0,0,0,2.138,39.8H7.1v5.464a.554.554,0,0,0,.86.462L16.907,39.8h5.347a2.14,2.14,0,0,0,2.138-2.138V31.17l8.2,5.446a.554.554,0,0,0,.861-.462V30.691h4.96a2.14,2.14,0,0,0,2.138-2.138V16.516A2.141,2.141,0,0,0,38.41,14.378ZM22.254,38.69H16.74a.553.553,0,0,0-.306.093L8.207,44.231V39.244a.554.554,0,0,0-.554-.554H2.138a1.03,1.03,0,0,1-1.03-1.03V25.624a1.031,1.031,0,0,1,1.03-1.03H22.254a1.031,1.031,0,0,1,1.03,1.03V37.66A1.03,1.03,0,0,1,22.254,38.69ZM39.439,28.553a1.03,1.03,0,0,1-1.03,1.03H32.9a.554.554,0,0,0-.554.554v4.984l-7.949-5.281V25.624a2.141,2.141,0,0,0-2.138-2.138H17.265V16.517a1.031,1.031,0,0,1,1.03-1.03H38.41a1.031,1.031,0,0,1,1.03,1.03Z" transform="translate(0 0)" fill="#fd9895"/>
    </g>
  </svg>
  `;
  
  const Message = ({ width, height }) => (<SvgCss xml= {xml} width= {width} height= {height} />
  );
  
  export default Message;
  