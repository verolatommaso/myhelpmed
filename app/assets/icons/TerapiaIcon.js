import React from "react";
import Svg, { Path, G, Rect } from "react-native-svg";

function TerapiaIcon({ fill, width, height }) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="29"
      height="31"
      viewBox="0 0 29 31"
    >
      <G id="medical_services_black_24dp" transform="translate(0 0.337)">
        <G id="Raggruppa_76" data-name="Raggruppa 76">
          <Rect
            id="Rettangolo_182"
            data-name="Rettangolo 182"
            width="29"
            height="31"
            transform="translate(0 -0.337)"
            fill="none"
          />
        </G>
        <G
          id="Raggruppa_78"
          data-name="Raggruppa 78"
          transform="translate(2 2)"
        >
          <G id="Raggruppa_77" data-name="Raggruppa 77">
            <Path
              id="Tracciato_635"
              data-name="Tracciato 635"
              d="M24.9,7.333H19.808V4.666A2.615,2.615,0,0,0,17.264,2H12.176A2.615,2.615,0,0,0,9.632,4.666V7.333H4.544A2.615,2.615,0,0,0,2,10V26a2.615,2.615,0,0,0,2.544,2.666H24.9A2.615,2.615,0,0,0,27.44,26V10A2.615,2.615,0,0,0,24.9,7.333ZM12.176,4.666h5.088V7.333H12.176ZM24.9,26H4.544V10H24.9Z"
              transform="translate(-2 -2)"
              fill={fill}
            />
            <Path
              id="Tracciato_636"
              data-name="Tracciato 636"
              d="M13,10H11v3H8v2h3v3h2V15h3V13H13Z"
              transform="translate(0.72 2.442)"
              fill={fill}
            />
          </G>
        </G>
      </G>
    </Svg>
  );
}

export default TerapiaIcon;
