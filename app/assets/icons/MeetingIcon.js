import React from "react";
import Svg, { Path, G, Rect } from "react-native-svg";

function MeetingIcon({ fill, width, height }) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="28.051"
      height="28"
      viewBox="0 0 28.051 28"
    >
      <G id="groups_black_24dp" transform="translate(0.009 -0.043)">
        <Rect
          id="Rettangolo_184"
          data-name="Rettangolo 184"
          width="28"
          height="28"
          transform="translate(0.042 0.043)"
          fill="none"
        />
        <G
          id="Raggruppa_79"
          data-name="Raggruppa 79"
          transform="translate(-0.009 6.959)"
        >
          <Path
            id="Tracciato_638"
            data-name="Tracciato 638"
            d="M4.643,14.125A2.322,2.322,0,1,0,2.322,11.8,2.328,2.328,0,0,0,4.643,14.125ZM5.955,15.4a8.105,8.105,0,0,0-1.312-.116,8.068,8.068,0,0,0-3.227.673A2.334,2.334,0,0,0,0,18.107v1.822H5.223V18.06A5.222,5.222,0,0,1,5.955,15.4Zm17.26-1.277A2.322,2.322,0,1,0,20.894,11.8,2.328,2.328,0,0,0,23.215,14.125Zm4.643,3.981a2.334,2.334,0,0,0-1.416-2.147A7.891,7.891,0,0,0,21.9,15.4a5.222,5.222,0,0,1,.731,2.658v1.869h5.223ZM18.851,14.88a12.122,12.122,0,0,0-4.922-1.045A12.317,12.317,0,0,0,9.007,14.88a3.468,3.468,0,0,0-2.043,3.18v1.869H20.894V18.06A3.468,3.468,0,0,0,18.851,14.88ZM9.367,17.608c.1-.267.151-.453,1.056-.8a9.777,9.777,0,0,1,7.011,0c.894.348.94.534,1.056.8Zm4.562-9.286a1.161,1.161,0,1,1-1.161,1.161,1.164,1.164,0,0,1,1.161-1.161m0-2.322a3.482,3.482,0,1,0,3.482,3.482A3.478,3.478,0,0,0,13.929,6Z"
            transform="translate(0 -6)"
            fill={fill}
          />
        </G>
      </G>
    </Svg>
  );
}

export default MeetingIcon;
