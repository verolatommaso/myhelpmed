import React from "react";

import { SvgCss } from "react-native-svg";

const xml = `
<svg xmlns="http://www.w3.org/2000/svg" width="55.165" height="46.199" viewBox="0 0 55.165 46.199">
<g id="Raggruppa_10" data-name="Raggruppa 10" transform="translate(-886.693 796.591)">
    <g id="Tracciato_6" data-name="Tracciato 6" transform="translate(902.652 -794.378)">
        <g id="Tracciato_8" data-name="Tracciato 8" transform="translate(0 0)">
            <path id="Tracciato_444" data-name="Tracciato 444"
                d="M1057.839-750.813l-8.649-8.651,5.975-5.975,5.828,5.825,14.439-14.439,5.975,5.977-17.263,17.263A4.459,4.459,0,0,1,1057.839-750.813Z"
                transform="translate(-1049.19 774.053)" fill="#eb7680" />
        </g>
    </g>
    <g id="Raggruppa_6" data-name="Raggruppa 6" transform="translate(886.693 -796.591)">
        <g id="Tracciato_9" data-name="Tracciato 9" transform="translate(0 0)">
            <path id="Tracciato_445" data-name="Tracciato 445"
                d="M914.5-750.392a27.812,27.812,0,0,1-27.809-27.814,27.811,27.811,0,0,1,6.945-18.385l7.614,6.713a17.661,17.661,0,0,0,1.583,24.926,17.661,17.661,0,0,0,24.926-1.584,17.662,17.662,0,0,0,4.111-8.457l9.985,1.83A27.8,27.8,0,0,1,914.5-750.392Z"
                transform="translate(-886.693 796.591)" fill="#6a7aff" />
        </g>
    </g>
</g>
</svg>

`;

const HatIcon = ({ width, height }) => (
  <SvgCss xml={xml} width={width} height={height} />
);

export default HatIcon;
