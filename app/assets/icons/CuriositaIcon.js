import React from "react";
import Svg, { Path, G, Rect } from "react-native-svg";

function CuriositaIcon({ fill, width, height }) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="29"
      height="29"
      viewBox="0 0 29 29"
    >
      <G id="preview_black_24dp" transform="translate(-0.459 -0.459)">
        <Rect
          id="Rettangolo_183"
          data-name="Rettangolo 183"
          width="29"
          height="29"
          transform="translate(0.459 0.459)"
          fill="none"
        />
        <Path
          id="Tracciato_637"
          data-name="Tracciato 637"
          d="M22.765,3H5.471A2.47,2.47,0,0,0,3,5.471V22.765a2.47,2.47,0,0,0,2.471,2.471H22.765a2.478,2.478,0,0,0,2.471-2.471V5.471A2.47,2.47,0,0,0,22.765,3Zm0,19.765H5.471V7.941H22.765Zm-8.647-10.5a6.086,6.086,0,0,1,5.361,3.088,6.2,6.2,0,0,1-10.722,0,6.086,6.086,0,0,1,5.361-3.088m0-1.853a7.962,7.962,0,0,0-7.412,4.941,8.029,8.029,0,0,0,14.823,0,7.962,7.962,0,0,0-7.412-4.941Zm0,6.794a1.853,1.853,0,1,1,1.853-1.853A1.85,1.85,0,0,1,14.118,17.206Z"
          transform="translate(0.687 0.687)"
          fill={fill}
        />
      </G>
    </Svg>
  );
}

export default CuriositaIcon;
