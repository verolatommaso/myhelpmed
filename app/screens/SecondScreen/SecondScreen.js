import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Brain from "../../assets/icons/Brain";
import Profile from "../../assets/icons/Profile";
import Hand from "../../assets/icons/Hand";
import Message from "../../assets/icons/Message";
import CalendarIcon from "../../assets/icons/CalendarIcon";
import Peapole from "../../assets/icons/Peapole";

const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "Il potere dell’organismo",
    hour: "Dalle 9:30 alle 10:30",
    day: 12,
    month: "Feb",
    image: () => <Peapole width={30} height={37} />,
  },
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28bfdd",
    title: "Il coraggio delle donne",
    hour: "Orario da definire",
    day: 2,
    month: "Gen",
    image: () => <Hand width={30} height={37} />,
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f688",
    title: "Meeting with Susan",
    hour: "Dalle 11:30 alle 12:30",
    day: 22,
    month: "Mar",
    image: () => <Message width={30} height={37} />,
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Dalle 17:30 alle 16:30",
    hour: "8:00",
    day: 23,
    month: "April",
    image: () => <CalendarIcon width={30} height={37} />,
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72dd",
    title: "Crescere insieme",
    hour: "Orario da definire",
    day: 1,
    month: "Mag",
    image: () => <Peapole width={30} height={37} />,
  },
];

const SecondScreen = () => {
  const Item = ({ title, hour, day, month, image }) => {
    return (
      <TouchableOpacity>
        <View
          style={{
            flexDirection: "row",
            alignItems: "flex-start",
          }}
        >
          <View
            style={{
              alignItems: "flex-start",
              marginTop: 11.5,
              marginHorizontal: 5,
              marginLeft: 20,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "flex-start",
                height: 78.83,
                width: 353,
                borderRadius: 5,
                backgroundColor: "rgba(255, 255, 255, 255)",
              }}
            >
              <View
                style={{
                  alignItems: "flex-start",
                }}
              >
                <View
                  style={{
                    alignItems: "flex-start",
                    paddingStart: 23,
                    paddingTop: 21.95,
                    minWidth: 78.83,
                    height: 78.83,
                    borderRadius: 5,
                    backgroundColor: "rgba(253, 242, 242, 255)",
                  }}
                >
                  <View
                    style={{
                      alignItems: "flex-start",
                    }}
                  >
                    {image()}
                  </View>
                </View>
              </View>
              <View
                style={{
                  alignItems: "flex-start",
                  marginStart: 26.67,
                  marginTop: 18.67,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      letterSpacing: -1.11,
                      color: "rgba(49, 49, 49, 255)",
                      fontFamily: "NunitoSans-Regular",
                      marginBottom: 7,
                    }}
                  >
                    {" "}
                    {title}{" "}
                  </Text>

                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "flex-start",
                      marginStart: 6.5,
                      marginTop: 11.25,
                    }}
                  >
                    <View
                      style={{
                        width: 16,
                        height: 16,
                        backgroundColor: "#ffffff",
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      color: "#707070",
                      marginStart: 0.5,
                      marginTop: -2.75,
                      fontFamily: "NunitoSans-Regular",
                    }}
                  >
                    {" "}
                    {hour}{" "}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  alignSelf: "center",
                  flexDirection: "column",
                  position: "absolute",
                  right: 30,
                }}
              >
                <Text
                  style={{
                    fontFamily: "NunitoSans-Regular",
                    fontSize: 14,
                    color: "#707070",
                  }}
                >
                  {month}
                </Text>
                <Text
                  style={{
                    marginTop: 10,
                    marginBottom: -5,
                    fontFamily: "NunitoSans-Bold",
                    fontSize: 20,
                    color: "#1D1D1D",
                  }}
                >
                  {day}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  const renderItem = ({ item }) => {
    return (
      <Item
        title={item.title}
        image={item.image}
        hour={item.hour}
        day={item.day}
        month={item.month}
        // onPress={() => setSelectedId(item.id)}
      />
    );
  };

  return (
    <View
      style={{
        alignItems: "flex-start",
      }}
    >
      <ScrollView contentContainerStyle={{ alignItems: "flex-start" }}>
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 16,
            letterSpacing: -0.14,
            textAlign: "center",
            color: "rgba(21, 33, 61, 255)",
            marginStart: 10,
            marginTop: 75,
          }}
        >
          Stanze attive{" "}
        </Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "flex-start",
          }}
        >
          <View
            style={{
              alignItems: "flex-start",
              marginTop: 11.5,
              marginHorizontal: 5,
              marginLeft: 20,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "flex-start",
                height: 78.83,
                borderRadius: 5,
                backgroundColor: "rgba(255, 255, 255, 255)",
              }}
            >
              <View
                style={{
                  alignItems: "flex-start",
                }}
              >
                <View
                  style={{
                    alignItems: "flex-start",
                    paddingStart: 23,
                    paddingTop: 21.95,
                    width: 78.83,
                    height: 78.83,
                    borderRadius: 5,
                    backgroundColor: "rgba(253, 242, 242, 255)",
                  }}
                >
                  <View
                    style={{
                      alignItems: "flex-start",
                    }}
                  >
                    <Brain width={30} height={37} />
                  </View>
                </View>
              </View>
              <View
                style={{
                  alignItems: "flex-start",
                  marginStart: 26.67,
                  marginTop: 18.67,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      letterSpacing: -1.11,
                      color: "rgba(49, 49, 49, 255)",
                      fontFamily: "NunitoSans-SemiBold",
                      marginBottom: 7,
                    }}
                  >
                    {" "}
                    Sintomi post terapia{" "}
                  </Text>

                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "flex-start",
                      marginStart: 6.5,
                      marginTop: 11.25,
                    }}
                  >
                    <View
                      style={{
                        width: 16,
                        height: 16,
                        backgroundColor: "#ffffff",
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      color: "#707070",
                      marginStart: 0.5,
                      marginTop: -2.75,
                      fontFamily: "NunitoSans-Regular",
                    }}
                  >
                    {" "}
                    Dalle 9:30 alle 10:30{" "}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  marginLeft: 40,
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center",
                }}
              >
                <Profile width={16} height={16} />
              </View>
              <View
                style={{
                  marginLeft: 4,
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    color: "#707070",
                    marginStart: 0.5,
                    fontFamily: "NunitoSans-Regular",
                    marginRight: 20,
                  }}
                >
                  20
                </Text>
              </View>
            </View>
          </View>
        </View>
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 16,
            letterSpacing: -0.14,
            textAlign: "center",
            color: "rgba(21, 33, 61, 255)",
            marginStart: 10,
            marginTop: 35,
          }}
        >
          {" "}
          In programma{" "}
        </Text>
        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          style={{ width: "100%" }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        />
      </ScrollView>
    </View>
  );
};

export default SecondScreen;

const styles = StyleSheet.create({});
