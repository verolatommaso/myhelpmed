import React, { useState } from "react";
import { View, FlatList, StyleSheet, Text, ScrollView } from "react-native";
import Articles1 from "../../assets/icons/Articles1";
import Articles2 from "../../assets/icons/Articles2";
import Articles3 from "../../assets/icons/Articles3";
import Articles4 from "../../assets/icons/Articles4";
import Articles5 from "../../assets/icons/Articles5";
import Post1 from "../../assets/icons/Post1";
import ChevronIcon from "../../assets/icons/ChevronIcon";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SearchBar, Overlay } from "react-native-elements";

const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "Il corpo è il tuo circolo",
    content: "Tumore del seno (carcinoma mammario o tumore alla mamm… ",
    durata: "5:00",
    image: () => <Articles2 width={78} height={78} />,
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Tumore mammario",
    content:
      "Una formazione di tessuto costituito da cellule che crescono in modo… ",
    durata: "8:00",
    image: () => <Articles1 width={78} height={78} />,
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72dd",
    title: "Terapia e sintomi",
    content: "Incontrollato e anomalo all’interno della ghiandola mammaria…",
    durata: "8:00",
    image: () => <Articles3 width={78} height={78} />,
  },

  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28bfdd",
    title: "La forza del proprio corpo",
    content:
      "Lo sapevi che il tumore al seno è fra le neoplasie più diffuse in Italia e … ",
    durata: "5:00",
    image: () => <Articles4 width={78} height={78} />,
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f688",
    title: "Armonia nel proprio corpo",
    content:
      "Lo sapevi che il tumore al seno è fra le neoplasie più diffuse in Italia e … ",
    durata: "8:00",
    image: () => <Articles5 width={78} height={78} />,
  },
];

const Item = ({ title, content, durata, image, toggleOverlay }) => (
  <TouchableOpacity
    style={{
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      flex: 1,
    }}
    onPress={toggleOverlay}
  >
    <View style={{ marginRight: 30, flex: 1 }}>{image()}</View>

    <View style={styles.item}>
      <Text style={styles.title} adjustsFontSizeToFit minimumFontScale={0.8}>
        {title}
      </Text>
      <Text style={styles.content} adjustsFontSizeToFit minimumFontScale={0.8}>
        {content}
      </Text>
      <Text style={styles.durata} adjustsFontSizeToFit minimumFontScale={0.8}>
        Durata {durata} Min
      </Text>
    </View>
  </TouchableOpacity>
);

const ThirdScreen = () => {
  const [search, setSearch] = useState("");
  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const renderItem = ({ item }) => (
    <Item
      title={item.title}
      content={item.content}
      durata={item.durata}
      image={item.image}
      toggleOverlay={toggleOverlay}
    />
  );

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainerStyle}
    >
      <View style={{ flex: 1, width: "100%" }}>
        <View style={{ width: "100%", paddingHorizontal: 20 }}>
          <SearchBar
            placeholder="Cerca"
            onChangeText={(newVal) => setSearch(newVal)}
            value={search}
            placeholderTextColor="#888"
            cancelButtonTitle="Cancel"
            containerStyle={{
              backgroundColor: "transparent",
              borderTopColor: "transparent",
              borderBottomColor: "transparent",
            }}
            backgroundColor={"white"}
            inputContainerStyle={{
              backgroundColor: "white",
              borderColor: "#6A7AFF",
            }}
            inputStyle={{
              borderTopColor: "#6A7AFF",
              borderBottomColor: "#6A7AFF",
              height: 20,
              backgroundColor: "green",
            }}
            round
            borderBottomColor={"red"}
          />
        </View>

        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          style={{ width: "100%", paddingHorizontal: 20 }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        />
        <Overlay isVisible={visible} fullScreen onBackdropPress={toggleOverlay}>
          <View
            style={{
              flex: 1,
              justifyContent: "flex-start",
            }}
          >
            <View style={{ position: "absolute", top: 70, left: 30 }}>
              <TouchableOpacity onPress={() => toggleOverlay()}>
                <ChevronIcon width={24} height={24} />
              </TouchableOpacity>
            </View>

            <View style={{ marginTop: "40%" }}>
              <Post1 height={157} />
            </View>
            <View>
              <View style={{ marginTop: 50, paddingLeft: 15 }}>
                <Text
                  style={styles.title}
                  adjustsFontSizeToFit
                  minimumFontScale={0.8}
                >
                  Armonia nel proprio corpo
                </Text>
                <Text
                  style={[styles.durata, { marginTop: 5, marginBottom: 20 }]}
                  adjustsFontSizeToFit
                  minimumFontScale={0.8}
                >
                  Durata 5:00 Min
                </Text>
                <Text
                  style={styles.content}
                  adjustsFontSizeToFit
                  minimumFontScale={0.8}
                >
                  Trasforma la sofferenza in arte. Cancella i segni della
                  malattia con il suo pennello. Cerca di dare alle donne operate
                  di tumore al seno una prospettiva diversa, di rinascita. Tutto
                  cominciò quando la pittrice, Annamaria Mazzini, fu costretta
                  ad affrontare lei stessa la terribile malattia. Che resta la
                  neoplasia più disgnosticata nelle donne: negli ultimi anni in
                  Italia si sono registrati mediamente circa 53mila nuovi casi
                  (quasi un tumore maligno ogni tre, il 29% del totale, è un
                  tumore mammario). Così Annamaria Mazzini elaborò e riuscì a
                  superare grazie alle tele la sua malattia.
                </Text>
              </View>
            </View>
          </View>
        </Overlay>
      </View>
    </ScrollView>
  );
};

export default ThirdScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },
  contentContainerStyle: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  item: {
    // backgroundColor: "#f9c2ff",
    marginVertical: 34,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flex: 4,
  },
  title: {
    fontSize: 20,
    fontFamily: "NunitoSans-Bold",
    color: "#15213D",
  },
  content: {
    fontSize: 15,
    fontFamily: "NunitoSans-Regular",
    color: "#707070",
  },
  durata: {
    fontSize: 14,
    fontFamily: "NunitoSans-SemiBold",
    color: "#15213D",
  },
});
