import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
import { ScrollView } from "react-native-gesture-handler";
import { Slider, Icon } from "react-native-elements";

import Cistite from "../../assets/icons/Cistite";
import Lacrimazione from "../../assets/icons/Lacrimazione";
import Acidita from "../../assets/icons/Acidita";


import { images, theme } from "../../constants";
const { COLORS, FONTS, SIZES } = theme;

const DATA = [
  {
    id: "0",
    title: "Cistite",
    image: (fill) => <Cistite height={43} width={36} fill={fill} />,
  },
  {
    id: "1",
    title: "Lacrimazione",
    image: (fill) => <Lacrimazione height={43} width={36} fill={fill} />,
  },
  {
    id: "2",
    title: "Acidità",
    image: (fill) => <Acidita height={43} width={36} fill={fill} />,
  },
];

const Item = ({ title, image, onPress, backgroundColor, textColor, fill }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        marginRight: 11,
      }}
    >
      <View
        style={{
          width: 115,
          height: 134,
          borderRadius: 15,
          backgroundColor: backgroundColor.backgroundColor,
        }}
      >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
          }}
        >
          {image(fill)}

          <Text
            style={{
              fontFamily: "NunitoSans-SemiBold",
              fontSize: 11,
              color: textColor.color,
              marginTop: 12.5,
            }}
          >
            {title}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const HomeScreen = ({ navigation }) => {
  const [selectedId, setSelectedId] = useState(0);

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "#6A7AFF" : "white";
    const color = item.id === selectedId ? "white" : "#15213D";
    const fill = item.id === selectedId ? "white" : "#6A7AFF";
    return (
      <Item
        title={item.title}
        image={item.image}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
        fill={fill}
        onPress={() => setSelectedId(item.id)}
      />
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ flex: 1, backgroundColor: "white" }}>
        <Calendar
          current={new Date()}
          onDayPress={(day) => {
            console.log("selected day", day);
          }}
          onDayLongPress={(day) => {
            console.log("selected day", day);
          }}
          onPressArrowLeft={(subtractMonth) => subtractMonth()}
          onPressArrowRight={(addMonth) => addMonth()}
          enableSwipeMonths={true}
          markedDates={{
            "2021-04-16": { selected: true, selectedColor: "#FD9895" },
            "2021-04-21": { selected: true, selectedColor: "#FD9895" },
            "2021-04-28": { selected: true, selectedColor: "#FD9895" },
          }}
        />
        <View>
          <Text
            style={{
              marginTop: 21,
              fontFamily: "NunitoSans-Regular",
              fontSize: 16,
              marginBottom: 36,
              marginLeft: 22,
            }}
          >
            Umore
          </Text>
          <Slider
            animateTransitions
            animationType="timing"
            maximumValue={100}
            minimumValue={10}
            minimumTrackTintColor="#6A7AFF"
            maximumTrackTintColor="#EDEAEA"
            orientation="horizontal"
            step={1}
            style={{
              width: 347,
              height: 20,
              alignSelf: "center",
              marginBottom: 53,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,

              elevation: 5,
            }}
            thumbStyle={{
              height: 40,
              width: 40,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,

              elevation: 5,
            }}
            thumbProps={{
              children: (
                <View
                  style={{
                    width: 15,
                    height: 15,
                    backgroundColor: "#6A7AFF",
                    borderRadius: 40,
                    alignItems: "center",
                    alignSelf: "center",
                    top: 11.5,
                  }}
                ></View>
              ),
            }}
            thumbTintColor="white"
            thumbTouchSize={{ width: 40, height: 40 }}
            trackStyle={{ height: 11, borderRadius: 20 }}
            value={23}
          />
          <Text
            style={{
              fontFamily: "NunitoSans-Bold",
              fontSize: 16,
              marginBottom: 11,
              marginLeft: 22,
            }}
          >
            Sintomi
          </Text>
          <TouchableOpacity>
            <FlatList
              data={DATA}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              style={{ width: SIZES.width, paddingVertical: 10 }}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              horizontal
              extraData={selectedId}
              contentContainerStyle={{ paddingLeft: 20 }}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
