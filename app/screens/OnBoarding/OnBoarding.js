import React from "react";
import {
  Animated,
  Image,
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from "react-native";
import { StackActions } from "@react-navigation/native";

// constants
import { images, theme } from "../../constants";
import AsyncStorage from "@react-native-community/async-storage";

const { onboarding1, onboarding2, onboarding3 } = images;

// theme
const { COLORS, FONTS, SIZES } = theme;

const onBoardings = [
  {
    title: "Omnium",
    description:
      "Omnium è una applicazione che ti aiuta nel percorso di accettazione e sostegno del tumore al seno.",
    img: onboarding1,
  },
  {
    title: "Monitora",
    description:
      "Con Omnium potrai interagire con la nostra comunità, monitorare il tuo trattamento personalizzandolo.",
    img: onboarding2,
  },
  {
    title: "Audit Room",
    description:
      "Partecipa alle numerose room tenute dai nostri specialisti, oppure creane una tu per condividere la tua esperienza.",
    img: onboarding3,
  },
];

const OnBoarding = ({ navigation }) => {
  const [completed, setCompleted] = React.useState(false);
  const saveData = async () => {
    const login = true;
    await AsyncStorage.setItem(
      "@authentication_data",
      JSON.stringify({
        login,
      })
    )
      .then(() => {
        navigation.dispatch({
          ...StackActions.replace("AppScreens"),
        });
      })
      .catch((e) => {
        console.warn(i18n.t("AsyncStorage.setItemError"), e);
      });
  };

  const scrollX = new Animated.Value(0);

  React.useEffect(() => {
    scrollX.addListener(({ value }) => {
      if (Math.floor(value / SIZES.width) === onBoardings.length - 1) {
        setCompleted(true);
      }
    });

    return () => scrollX.removeListener();
  }, []);

  // Render

  function renderContent() {
    return (
      <Animated.ScrollView
        horizontal
        pagingEnabled
        scrollEnabled
        decelerationRate={0}
        scrollEventThrottle={16}
        snapToAlignment="center"
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
      >
        {onBoardings.map((item, index) => (
          <View
            //center
            //bottom
            key={`img-${index}`}
            style={styles.imageAndTextContainer}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Image
                source={item.img}
                resizeMode="cover"
                style={{
                  width: "100%",
                  height: "100%",
                }}
              />
            </View>
            <View
              style={{
                position: "absolute",
                bottom: 180,
                justifyContent: "center",
                alignItems: "center",
                alignSel: "center",
                flex: 1,
                paddingHorizontal: 50
              }}
            >
              <Text
                style={{
                  fontFamily: "NunitoSans-Bold",
                  fontSize: 25,
                  textAlign: "center",
                  color: "rgba(21, 33, 61, 255)",
                  marginBottom: 20,
                }}
              >
                {item.title}
              </Text>

              <Text
                style={{
                  fontFamily: "NunitoSans-Regular",
                  fontSize: 16,
                  textAlign: "center",
                  color: "rgba(110, 110, 110, 255)",
                }}
              >
                {item.description}
              </Text>
            </View>
            {/* Button */}
            {index === 2 && (
              <View
                style={{
                  width: 354,
                  height: 56,
                  bottom: 82,
                  position: "absolute",
                  alignSelf: "center",
                  borderRadius: 14,
                  backgroundColor: COLORS.blue,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <TouchableOpacity
                  onPress={index === 2 ? () => saveData() : () => {}}
                >
                  <Text
                    style={{
                      fontFamily: "NunitoSans-SemiBold",
                      fontSize: 18,
                      color: "white",
                    }}
                  >
                    Continua
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        ))}
      </Animated.ScrollView>
    );
  }
  function renderDots() {
    const dotPosition = Animated.divide(scrollX, SIZES.width);

    return (
      <View style={styles.dotsContainer}>
        {onBoardings.map((item, index) => {
          const opacity = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [0.3, 1, 0.3],
            extrapolate: "clamp",
          });

          const dotSize = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [SIZES.base, 17, SIZES.base],
            extrapolate: "clamp",
          });

          return (
            <Animated.View
              key={`dot-${index}`}
              opacity={opacity}
              style={[styles.dot, { width: dotSize, height: dotSize }]}
            />
          );
        })}
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      {renderContent()}
      <View style={styles.dotsRootContainer}>{renderDots()}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.white,
  },
  imageAndTextContainer: {
    width: SIZES.width,
  },
  dotsRootContainer: {
    position: "absolute",
    bottom: 36,
  },
  dotsContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  dot: {
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.blue,
    marginHorizontal: SIZES.radius / 2,
  },
});

export default OnBoarding;
