import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import AppIcon from "../../assets/icons/AppIcon";

const ConditionScreen = ({ navigation }) => {
  return (
    <View style={styles.mainContainer}>
      <AppIcon width={55} height={46} />
      <Text style={styles.textContainer}>Benvenuto</Text>
      <Text style={{ marginBottom: 100 }}>
        <Text style={styles.textBody}>Accetto la</Text>
        <Text style={styles.textOrange}>{""} informativa sulla privacy </Text>
        <Text style={styles.textBody}>
          e le condizioni d’uso. Accetto il trattamento dei miei dati personali
          sanitari personali al fine di ricevere le funzioni dell’ app di
          omnium. Maggiori informazioni possono essere trovate nella informativa
          sulla privacy. Accetto che i partner integrati possano ricevere i miei
          dati personali. Questo aiuta Omnium a raggiungere me e più persone
          come me per diffondere la conoscenza globalmente di questa
          problematica, tali dati condivisi sono rigorosamente limitati.{" "}
        </Text>
      </Text>
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => {
          navigation.navigate("OnBoarding");
        }}
      >
        <Text style={styles.buttonTextStyle}>Accetta condizioni</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ConditionScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 36,
  },
  buttonStyle: {
    backgroundColor: "#6A7AFF",
    width: "100%",
    height: 56,
    borderRadius: 14,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonTextStyle: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: 18,
    color: "#FFFFFF",
    textAlign: "center",
  },
  textContainer: {
    fontFamily: "Poppins-SemiBold",
    fontSize: 30,
    letterSpacing: -0.14,
    textAlign: "center",
    color: "rgba(21, 33, 61, 255)",
    marginBottom: 30,
    marginTop: 20,
  },
  textBody: {
    fontFamily: "NunitoSans-Regular",
    fontSize: 16,
    letterSpacing: -0.14,
    color: "rgba(112, 112, 112, 255)",
  },
  textOrange: {
    fontFamily: "NunitoSans-Regular",
    fontSize: 16,
    letterSpacing: -0.14,
    color: "rgba(253, 152, 149, 255)",
  },
});
